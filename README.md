# Jak_miec_biala_miedz

Projekt zespołu RS-Team mający na celu optymalizację jakości produkcji miedzi.

## Opis elementów:
- requirements.txt - środowisko do pracy z naszym kodem

- Dashboard - prototyp programu predykującego i wspomagającego optymalizację jakości miedzi (na ten moment predykcje wykonują się tylko na ustalonych z góry cyklach, jeśli chcemy przy pomocy programu zwizualizować predykcję dla innych cykli musimy je dodać do listy to_predict w pliku predictor.py w postaci krotki: (nr grupy, data rozpoczęcia cyklu)

- Przygotowanie modeli - przykłady naszych podejść do przygotowywania różnych modeli predykcyjnych dla problemu

- Walidacja modeli - Narzędzia umożliwiające testowanie modeli na naszym (lub nowym) zbiorze testowym

## Ważne informacje:
- We wszystkich folderach gdzie występuje podfolder 'data' nalezy uzupełnić go danymi w formie takiej jak dostarczona. W tych folderach znajdują się również pliki file_names.json mogące pomóc dostosowac nazwy plików (w przypadku wygenerowanie innych nazw). Nie umieszczaliśmy danych w repozytorium z uwagi na ich poufny charakter.

- Dostarczone notebooki nie stanowią całości wykonywanych analiz, ale pozwalają na zapoznanie się z naszym podejściem do problemu.
