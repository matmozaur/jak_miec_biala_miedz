import pandas as pd
import json


with open('model_validator/file_names.json') as f:
    FILE_NAMES = json.load(f)


def prepare_1(data, circulations):
    name = FILE_NAMES['file_1']
    for k in data[name].keys():
        tmp = data[name][k] 
        tmp['Data'] = pd.to_datetime(tmp['Data'])
        tmp.set_index('Data', inplace=True)
        data[name][k] = tmp
    for k in range(1,13):
        for k1 in data[name].keys():
            circulations[k]['Temperatura {}'.format(k1)] =\
            data[name][k1]['Obieg {}'.format(k)]
            
            
def prepare_2(data, circulations):
    name = FILE_NAMES['file_2']
    for k in data[name].keys():
        tmp = data[name][k] 
        tmp['Data'] = pd.to_datetime(tmp['Data'])
        tmp.set_index('Data', inplace=True)
        data[name][k] = tmp
    for k in range(1,13):
        for k1 in data[name].keys():
            circulations[k]['Poziom elektrolitu {}'.format(k1)] =\
            data[name][k1]['Obieg {}'.format(k)]
            

def prepare_3(data, circulations):
    name = FILE_NAMES['file_3']
    for k in data[name].keys():
        tmp = data[name][k] 
        tmp['Data'] = tmp['Data'].astype(str)+ ' ' + tmp['GODZINA'].astype(str)+ ':00:00'
        tmp['Data'] = pd.to_datetime(tmp['Data'])
        tmp.drop('GODZINA', axis=1, inplace=True)
        tmp.set_index('Data', inplace=True)
        data[name][k] = tmp
    for k in range(1,13):
        for k1 in data[name].keys():
            for k2 in data[name][k1].columns:
                if '_{}_'.format(k) in k2:
                    circulations[k][k2[:k2.find('przepływ_')] +'przepływ'] =\
                    data[name][k1][k2]


def prepare_4(data, circulations):
    name = FILE_NAMES['file_4']
    for k in data[name].keys():
        tmp = data[name][k] 
        tmp['Data'] = pd.to_datetime(tmp['Data'])
        tmp.set_index('Data', inplace=True)
        data[name][k] = tmp
    for k in range(1,13):
        for k1 in data[name].keys():
            for k2 in data[name][k1].drop('obieg', axis=1).columns:
                circulations[k][k2] =\
                data[name][k1][data[name][k1]['obieg']==k][k2]
            

def prepare_10(data, circulations):
    name = FILE_NAMES['file_10']

    for k in data[name].keys():
        tmp = data[name][k] 
        tmp['data_analizy'] = pd.to_datetime(tmp['data_analizy'])
        tmp.set_index('data_analizy', inplace=True)
        data[name][k] = tmp

    for k in range(1,13):
        for k1 in data[name].keys():
            for k2 in data[name][k1].columns:
                if 'Obieg{}'.format(k) == k2:
                    circulations[k][k1] =\
                    data[name][k1][k2]
            
            
def prepare_circulation(data):
    circulations = {}
    idx = pd.to_datetime(data[FILE_NAMES['file_1']]['Zbiorniki naporowe']['Data'])
    for k in range(1, 13):
        circulations[k] = pd.DataFrame(index=idx)
    prepare_1(data, circulations)
    prepare_2(data, circulations)
    prepare_3(data, circulations)
    prepare_4(data, circulations)
    prepare_10(data, circulations)
    return circulations


def prepare_circuits(data):
    circuits = {}
    name = FILE_NAMES['file_5']
    for k in data[name].keys():
        tmp = data[name][k] 
        tmp['Data/godzina'] = pd.to_datetime(tmp['Data/godzina'])
        tmp.set_index('Data/godzina', inplace=True)
        data[name][k] = tmp

    for k in range(1,7):
        circuits[k] = data[name]['Obwody prądowe'][['Obwód {}'.format(k)]]
        circuits[k].columns = ['Obwód {} natężenie'.format(k)]
    return circuits
