import numpy as np
import pickle5 as pickle

from model_validator.utils_group_cycles import *

obiegi_cyrkulacyjne = {
    1: list(range(3, 9)),
    2: [1, 2] + list(range(9, 17)),
    3: list(range(17, 25)),
    4: list(range(25, 33)),
    5: list(range(33, 41)),
    6: list(range(41, 49)),
    7: list(range(49, 57)),
    8: list(range(57, 65)),
    9: list(range(65, 73)),
    10: list(range(73, 81)),
    11: list(range(81, 89)),
    12: list(range(89, 97))
}

LIMITS = {
    'Ag': 10,
    'Pb': 1,
    'Fe': 2,
    'Ni': 2,
    'S': 4,
    'Zn': 1,
}


def get_source_wytop(numer_wytopu):
    return str(numer_wytopu)[0]


def obiegi_mapper(numer_grupy):
    for key, value in obiegi_cyrkulacyjne.items():
        if numer_grupy in value:
            return key


def pik_dol(tiomocznik_df):
    tiomocznik_mean = tiomocznik_df['Tiomocznik_przepływ'].mean()
    tiomocznik_df['diff'] = tiomocznik_df['Tiomocznik_przepływ'].diff()
    return tiomocznik_df[(tiomocznik_df['diff'] < -2) & \
                         (tiomocznik_df['Tiomocznik_przepływ'] < (tiomocznik_mean - 2))].shape[0]


def piki(tiomocznik_df):
    tiomocznik_df['diff'] = tiomocznik_df['Tiomocznik_przepływ'].diff()
    wzrosty = tiomocznik_df[tiomocznik_df['diff'] > 2].shape[0]
    spadki = tiomocznik_df[tiomocznik_df['diff'] < -2].shape[0]
    return (wzrosty + spadki) // 2


def kiedy_lewarowanie(data):
    elektrolit = data[['Elektrolit [m3]']].dropna()
    elektrolit = elektrolit.reset_index(drop=True).reset_index(drop=False)[:4]
    elektrolit = elektrolit[elektrolit['Elektrolit [m3]'] > 0]
    try:
        return elektrolit['index'].iloc[0]
    except IndexError:
        return -1


col_chem = ['Analiza chemiczna anod  ANALIZA Pb [%]',
            'Analiza chemiczna anod  ANALIZA As [%]',
            'Analiza chemiczna anod  ANALIZA Ni [%]',
            'Analiza chemiczna anod  ANALIZA S [%]',
            'Analiza chemiczna anod  ANALIZA Sb [%]',
            'Analiza chemiczna anod  ANALIZA O2 [%]',
            'Analiza chemiczna anod  ANALIZA Bi [%]',
            'Analiza chemiczna anod  ANALIZA Sn [%]',
            'Analiza chemiczna anod  ANALIZA Zn [%]',
            'Analiza chemiczna anod  ANALIZAAg [%]']


def prepare_data_model_miedzi_Cu_CATH1_NSP(location='model_validator/data'):
    data, groups_map = prepare_all_data(location, fill=False)
    for k, v in groups_map.items():
        v['data'].columns = [re.sub('Obwód \d', '', c) for c in v['data'].columns]
    name = '8.9.11Klasyfikacja jakościowa katod,skład chemiczny katod i anod w grupach(każda grupa produkcyjna-stosy.xlsx'
    data[name]['Grupy produkcyjne']['Klasyfikacja wszystkie'] = \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod HMG-S [stosy]'] + \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod Cu- CATH 1 Z [stosy]'] + \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod Cu-CATH 2 [stosy]']

    cycles = []
    for i, row in data[name]['Grupy produkcyjne'].iterrows():
        print(i, end='\r')
        if row['Klasyfikacja wszystkie'] > 0 and row['TRYB'] == 'PROD':
            cond = True
            for k, v in LIMITS.items():
                if v + 0.5 <= row['Analiza chemiczna katod ANALIZA {} [ppm]'.format(k)]:
                    cond = False
            cycles.append(
                {
                    'params': (row['NUMER GRUPY'], row['DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd'],
                               row['DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']),
                    'percent': row['Klasyfikacja jakościowa katod HMG-S [stosy]'] / row['Klasyfikacja wszystkie'],
                    'match_cond': cond,
                    **get_cycle_info(row['NUMER GRUPY'], row['DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd'],
                                     row['DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd'], data=data, groups_map=groups_map)
                }
            )

    data[name]['Dane produkcji katod'] = data[name]['Dane produkcji katod'][
        ~data[name]['Dane produkcji katod']['NUMER WYTOPU 6 6'].isna()]
    data[name]['Dane produkcji katod']['NUMER GRUPY'] = data[name]['Dane produkcji katod']['NUMER GRUPY'].astype(int)
    data[name]['Dane produkcji katod'] = data[name]['Dane produkcji katod'][
        data[name]['Dane produkcji katod']['TRYB'] == 'PROD']

    mins = data[name]['Dane produkcji katod'][['Analiza chemiczna anod  ANALIZA Pb [%]',
                                               'Analiza chemiczna anod  ANALIZA As [%]',
                                               'Analiza chemiczna anod  ANALIZA Ni [%]',
                                               'Analiza chemiczna anod  ANALIZA S [%]',
                                               'Analiza chemiczna anod  ANALIZA Sb [%]',
                                               'Analiza chemiczna anod  ANALIZA O2 [%]',
                                               'Analiza chemiczna anod  ANALIZA Bi [%]',
                                               'Analiza chemiczna anod  ANALIZA Sn [%]',
                                               'Analiza chemiczna anod  ANALIZAAg [%]',
                                               'NUMER GRUPY',
                                               'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
                                               'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']].groupby(
        ['NUMER GRUPY', 'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
         'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']).min().reset_index(drop=False)
    mins.columns = list(mins.columns[:3]) + [c + '_min' for c in mins.columns[3:]]

    maxs = data[name]['Dane produkcji katod'][['Analiza chemiczna anod  ANALIZA Pb [%]',
                                               'Analiza chemiczna anod  ANALIZA As [%]',
                                               'Analiza chemiczna anod  ANALIZA Ni [%]',
                                               'Analiza chemiczna anod  ANALIZA S [%]',
                                               'Analiza chemiczna anod  ANALIZA Sb [%]',
                                               'Analiza chemiczna anod  ANALIZA O2 [%]',
                                               'Analiza chemiczna anod  ANALIZA Bi [%]',
                                               'Analiza chemiczna anod  ANALIZA Sn [%]',
                                               'Analiza chemiczna anod  ANALIZAAg [%]',
                                               'NUMER GRUPY',
                                               'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
                                               'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']].groupby(
        ['NUMER GRUPY', 'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
         'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']).max().reset_index(drop=False)
    maxs.columns = list(maxs.columns[:3]) + [c + '_max' for c in maxs.columns[3:]]

    ranges = data[name]['Dane produkcji katod'][['Analiza chemiczna anod  ANALIZA Pb [%]',
                                                 'Analiza chemiczna anod  ANALIZA As [%]',
                                                 'Analiza chemiczna anod  ANALIZA Ni [%]',
                                                 'Analiza chemiczna anod  ANALIZA S [%]',
                                                 'Analiza chemiczna anod  ANALIZA Sb [%]',
                                                 'Analiza chemiczna anod  ANALIZA O2 [%]',
                                                 'Analiza chemiczna anod  ANALIZA Bi [%]',
                                                 'Analiza chemiczna anod  ANALIZA Sn [%]',
                                                 'Analiza chemiczna anod  ANALIZAAg [%]',
                                                 'NUMER GRUPY',
                                                 'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
                                                 'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']].groupby(
        ['NUMER GRUPY', 'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
         'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']).apply(lambda g: g[['Analiza chemiczna anod  ANALIZA Pb [%]',
                                                                  'Analiza chemiczna anod  ANALIZA As [%]',
                                                                  'Analiza chemiczna anod  ANALIZA Ni [%]',
                                                                  'Analiza chemiczna anod  ANALIZA S [%]',
                                                                  'Analiza chemiczna anod  ANALIZA Sb [%]',
                                                                  'Analiza chemiczna anod  ANALIZA O2 [%]',
                                                                  'Analiza chemiczna anod  ANALIZA Bi [%]',
                                                                  'Analiza chemiczna anod  ANALIZA Sn [%]',
                                                                  'Analiza chemiczna anod  ANALIZAAg [%]']].max() -
                                                               g[['Analiza chemiczna anod  ANALIZA Pb [%]',
                                                                  'Analiza chemiczna anod  ANALIZA As [%]',
                                                                  'Analiza chemiczna anod  ANALIZA Ni [%]',
                                                                  'Analiza chemiczna anod  ANALIZA S [%]',
                                                                  'Analiza chemiczna anod  ANALIZA Sb [%]',
                                                                  'Analiza chemiczna anod  ANALIZA O2 [%]',
                                                                  'Analiza chemiczna anod  ANALIZA Bi [%]',
                                                                  'Analiza chemiczna anod  ANALIZA Sn [%]',
                                                                  'Analiza chemiczna anod  ANALIZAAg [%]']].min()).reset_index(
        drop=False)
    ranges.columns = list(ranges.columns[:3]) + [c + '_range' for c in ranges.columns[3:]]

    data[name]['Dane produkcji katod']['zrodlo'] = data[name]['Dane produkcji katod']['NUMER WYTOPU 6 6'].apply(
        get_source_wytop)
    liczba_zrodel = data[name]['Dane produkcji katod'][['NUMER GRUPY', 'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
                                                        'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd', 'zrodlo']].groupby(
        ['NUMER GRUPY', 'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
         'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']).nunique().reset_index(
        drop=False)

    target = pd.DataFrame([int(c['match_cond']) for c in cycles], columns=['match_cond'])

    input_data = pd.DataFrame([np.concatenate([np.array(c['params']), c['input_params'].values[0]]) for c in cycles])
    input_data.columns = ['NUMER GRUPY',
                          'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
                          'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd'] + list(cycles[0]['input_params'].columns)
    input_data['CYKL'] = input_data['CYKL'].astype(int).astype(str)
    input_data = input_data.merge(ranges).merge(mins).merge(maxs).merge(liczba_zrodel)
    input_data['obieg_cyrkulacyjny'] = input_data['NUMER GRUPY'].apply(obiegi_mapper)
    input_data = input_data.drop(columns=['NUMER GRUPY', 'TRYB',
                                          'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
                                          'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd',
                                          'NUMER WYTOPU 6 6',
                                          'OPIS WYTOPU 7 7',
                                          'ZRÓDLO WYTOPU 8 8',
                                          'MASA WYTOPU 9 Mg',
                                          'ENERGIA 0 kWh',
                                          'WYDAJNOŚĆ 1 %'
                                          ], errors='ignore')

    input_data.columns = [c.replace(',', '') for c in input_data.columns]
    input_data.columns = [c.replace('[', '') for c in input_data.columns]
    input_data.columns = [c.replace(']', '') for c in input_data.columns]
    input_data = input_data[~input_data['CYKL'].isna()]
    input_data = input_data.fillna(value=input_data.mean())
    input_data = input_data[['CYKL',
                             'Analiza chemiczna anod  ANALIZA Pb %',
                             'Analiza chemiczna anod  ANALIZA As %',
                             'Analiza chemiczna anod  ANALIZA Ni %',
                             'Analiza chemiczna anod  ANALIZA S %',
                             'Analiza chemiczna anod  ANALIZA Sb %',
                             'Analiza chemiczna anod  ANALIZA O2 %',
                             'Analiza chemiczna anod  ANALIZA Bi %',
                             'Analiza chemiczna anod  ANALIZA Sn %',
                             'Analiza chemiczna anod  ANALIZAAg %',
                             'Analiza chemiczna anod  ANALIZA Pb %_range',
                             'Analiza chemiczna anod  ANALIZA As %_range',
                             'Analiza chemiczna anod  ANALIZA Ni %_range',
                             'Analiza chemiczna anod  ANALIZA S %_range',
                             'Analiza chemiczna anod  ANALIZA Sb %_range',
                             'Analiza chemiczna anod  ANALIZA O2 %_range',
                             'Analiza chemiczna anod  ANALIZA Bi %_range',
                             'Analiza chemiczna anod  ANALIZA Sn %_range',
                             'Analiza chemiczna anod  ANALIZAAg %_range',
                             'Analiza chemiczna anod  ANALIZA Pb %_min',
                             'Analiza chemiczna anod  ANALIZA As %_min',
                             'Analiza chemiczna anod  ANALIZA Ni %_min',
                             'Analiza chemiczna anod  ANALIZA S %_min',
                             'Analiza chemiczna anod  ANALIZA Sb %_min',
                             'Analiza chemiczna anod  ANALIZA O2 %_min',
                             'Analiza chemiczna anod  ANALIZA Bi %_min',
                             'Analiza chemiczna anod  ANALIZA Sn %_min',
                             'Analiza chemiczna anod  ANALIZAAg %_min',
                             'Analiza chemiczna anod  ANALIZA Pb %_max',
                             'Analiza chemiczna anod  ANALIZA As %_max',
                             'Analiza chemiczna anod  ANALIZA Ni %_max',
                             'Analiza chemiczna anod  ANALIZA S %_max',
                             'Analiza chemiczna anod  ANALIZA Sb %_max',
                             'Analiza chemiczna anod  ANALIZA O2 %_max',
                             'Analiza chemiczna anod  ANALIZA Bi %_max',
                             'Analiza chemiczna anod  ANALIZA Sn %_max',
                             'Analiza chemiczna anod  ANALIZAAg %_max',
                             'zrodlo',
                             'obieg_cyrkulacyjny']]
    input_data['CYKL'] = input_data['CYKL'].astype(int)
    input_data['zrodlo'] = input_data['zrodlo'].astype(int)
    input_data['obieg_cyrkulacyjny'] = input_data['obieg_cyrkulacyjny'].astype(int)
    return input_data, target


def prepare_data_model_miedzi_bialej_pelen_cykl(location='model_validator/data'):
    data, groups_map = prepare_all_data(location, fill=False)
    for k, v in groups_map.items():
        v['data'].columns = [re.sub('Obwód \d', '', c) for c in v['data'].columns]
    name = '8.9.11Klasyfikacja jakościowa katod,skład chemiczny katod i anod w grupach(każda grupa produkcyjna-stosy.xlsx'

    data[name]['Grupy produkcyjne']['Klasyfikacja wszystkie'] = \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod HMG-S [stosy]'] + \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod Cu- CATH 1 Z [stosy]'] + \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod Cu-CATH 2 [stosy]']

    cycles = []
    for i, row in data[name]['Grupy produkcyjne'].iterrows():
        print(i, end='\r')
        if row['Klasyfikacja wszystkie'] > 0 and row['TRYB'] == 'PROD':
            cond = True
            for k, v in LIMITS.items():
                if v + 0.5 <= row['Analiza chemiczna katod ANALIZA {} [ppm]'.format(k)]:
                    cond = False
            cycles.append(
                {
                    'params': (row['NUMER GRUPY'], row['DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd'],
                               row['DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']),
                    'percent': row['Klasyfikacja jakościowa katod HMG-S [stosy]'] / row['Klasyfikacja wszystkie'],
                    'match_cond': cond,
                    **get_cycle_info(row['NUMER GRUPY'], row['DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd'],
                                     row['DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd'], data=data, groups_map=groups_map)
                }
            )

    target = pd.DataFrame([[c['percent'], int(c['match_cond'])] for c in cycles], columns=['percent', 'match_cond'])

    tmp_x_1 = pd.DataFrame([c['input_params'][['CYKL']].values[0] for c in cycles], columns=['CYKL'])
    tmp_x_2 = pd.DataFrame([c['input_params'][col_chem].max().values for c in cycles],
                           columns=['{} _max'.format(c) for c in col_chem])
    tmp_x_3 = pd.DataFrame([c['input_params'][col_chem].min().values for c in cycles],
                           columns=['{} _min'.format(c) for c in col_chem])
    tmp_x_4 = pd.DataFrame([c['input_params'][col_chem].max().values - \
                            c['input_params'][col_chem].min().values for c in cycles],
                           columns=['{} _range'.format(c) for c in col_chem])
    tmp_x_5 = pd.DataFrame([[c['cycle_run']['Poziom elektrolitu Zbiorniki naporowe'].max(),
                             c['cycle_run']['Poziom elektrolitu Zbiorniki naporowe'].mean(),
                             c['cycle_run']['Poziom elektrolitu Zbiorniki naporowe'][4 * 24:-4 * 24].mean(),
                             c['cycle_run']['Poziom elektrolitu Zbiorniki naporowe'][:-4 * 24].mean()] for c in cycles],
                           columns=['Poziom elektrolitu Zbiorniki naporowe _max',
                                    'Poziom elektrolitu Zbiorniki naporowe_mean',
                                    'Poziom elektrolitu Zbiorniki naporowe _ph2_mean',
                                    'Poziom elektrolitu Zbiorniki naporowe _ph1_2_mean'])
    tmp_x_6 = pd.DataFrame([[c['cycle_run']['Poziom elektrolitu Zbiorniki cyrkulacyjne'].min(),
                             c['cycle_run']['Poziom elektrolitu Zbiorniki cyrkulacyjne'].max() -
                             c['cycle_run']['Poziom elektrolitu Zbiorniki cyrkulacyjne'].min()] for c in cycles],
                           columns=['Poziom elektrolitu Zbiorniki cyrkulacyjne _min',
                                    'Poziom elektrolitu Zbiorniki cyrkulacyjne _range'])
    tmp_x_7 = pd.DataFrame([[c['cycle_run']['Temperatura Zbiorniki naporowe'][4 * 24:-4 * 24].mean(),
                             c['cycle_run']['Temperatura Zbiorniki naporowe'].mean(),
                             c['cycle_run']['Temperatura Zbiorniki cyrkulacyjne'].max() - \
                             c['cycle_run']['Temperatura Zbiorniki cyrkulacyjne'].min()] for c in cycles],
                           columns=['Temperatura Zbiorniki naporowe _ph2_mean',
                                    'Temperatura Zbiorniki naporowe_mean',
                                    'Temperatura Zbiorniki cyrkulacyjne _range'])
    tmp_x_8 = pd.DataFrame([[c['cycle_run']['NaCl_przepływ'].quantile(0.25),
                             c['cycle_run']['Kondensat [m3]'].sum(),
                             c['cycle_run']['NaCl [kg]'].max() - \
                             c['cycle_run']['NaCl [kg]'].min()]
                            for c in cycles],
                           columns=['NaCl_przepływ _q25',
                                    'Kondensat [m3] _sum',
                                    'NaCl [kg] _range'])
    tmp_x_9 = pd.DataFrame([[c['cycle_run']['H2SO4'].min(),
                             c['cycle_run']['H2SO4'].mean(),
                             c['cycle_run']['H2SO4'].max() - \
                             c['cycle_run']['H2SO4'].min(),
                             c['cycle_run']['H2SO4'][4 * 24:-4 * 24].mean(),
                             c['cycle_run']['H2SO4'][:-4 * 24].mean()] for c in cycles],
                           columns=['H2SO4 _min',
                                    'H2SO4 _mean',
                                    'H2SO4 _range',
                                    'H2SO4 _phase2_mean',
                                    'H2SO4 _phase1_2_mean'])
    tmp_x_10 = pd.DataFrame([[c['cycle_run']['Cu'].min(),
                              c['cycle_run']['Cu'].max() - \
                              c['cycle_run']['Cu'].min(),
                              c['cycle_run']['Cl'].max(),
                              c['cycle_run']['Cl'].mean(),
                              c['cycle_run']['Cl'].max() - \
                              c['cycle_run']['Cl'].min(),
                              c['cycle_run']['Cl'][4 * 24:-4 * 24].mean(),
                              c['cycle_run']['Cl'][:-4 * 24].mean()] for c in cycles],
                            columns=['Cu _min',
                                     'Cu _range',
                                     'Cl _max',
                                     'Cl _mean',
                                     'Cl _range',
                                     'Cl _phase2_mean',
                                     'Cl _phase1_2_mean'])
    tmp_x_11 = pd.DataFrame([[kiedy_lewarowanie(c['cycle_run'])] for c in cycles],
                            columns=['kiedy_lewarowanie'])
    tmp_x_12 = pd.DataFrame([[c['cycle_run']['Tiomocznik_przepływ'].sum(),
                              piki(c['cycle_run'][['Tiomocznik_przepływ']]),
                              pik_dol(c['cycle_run'][['Tiomocznik_przepływ']])] for c in cycles],
                            columns=['Tiomocznik_przepływ _sum',
                                     'piki',
                                     'pik_dol'])
    tmp_x_13 = pd.DataFrame([[c['cycle_run']['Ni'][0],
                              c['cycle_run']['Ni'].max(),
                              c['cycle_run']['Bi'][-1],
                              c['cycle_run']['Bi'][-4 * 24:].min(),
                              c['cycle_run']['Bi'].min(),
                              c['cycle_run']['As'].min()] for c in cycles],
                            columns=['Ni _open',
                                     'Ni _max',
                                     'Bi _close',
                                     'Bi _phase3_mean',
                                     'Bi _min',
                                     'As _min'])
    tmp_x_14 = pd.DataFrame([[c['cycle_run']['Napięcie'][-4 * 24:].mean(),
                              c['cycle_run']['Napięcie'][-1],
                              c['cycle_run']['Napięcie'].quantile(0.75),
                              c['cycle_run']['Napięcie'].mean()] for c in cycles],
                            columns=['Napięcie _phase3_mean',
                                     'Napięcie _close',
                                     'Napięcie _q75',
                                     'Napięcie _mean'])
    input_data = pd.concat([tmp_x_1, tmp_x_2, tmp_x_3, tmp_x_4, tmp_x_5, tmp_x_6, tmp_x_7, tmp_x_8, tmp_x_9, tmp_x_10,
                            tmp_x_11, tmp_x_12, tmp_x_13, tmp_x_14], axis=1)

    with open('model_validator/models/rf_model_sc.pickle', 'rb') as f:
        scaler = pickle.load(f)
    input_data = input_data.fillna(0)
    cols = input_data.columns
    input_data = pd.DataFrame(scaler.transform(input_data))
    input_data.columns = cols
    return input_data, (target['percent'] > 0.9999).astype(int)


def prepare_data_model_miedzi_bialej_po_4_dniach(location='model_validator/data'):
    data, groups_map = prepare_all_data(location, fill=False)
    for k, v in groups_map.items():
        v['data'].columns = [re.sub('Obwód \d', '', c) for c in v['data'].columns]
    name = '8.9.11Klasyfikacja jakościowa katod,skład chemiczny katod i anod w grupach(każda grupa produkcyjna-stosy.xlsx'
    data[name]['Grupy produkcyjne']['Klasyfikacja wszystkie'] = \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod HMG-S [stosy]'] + \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod Cu- CATH 1 Z [stosy]'] + \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod Cu-CATH 2 [stosy]']
    cycles = []

    for i, row in data[name]['Grupy produkcyjne'].iterrows():
        print(i, end='\r')
        if row['Klasyfikacja wszystkie'] > 0 and row['TRYB'] == 'PROD':
            cond = True
            for k, v in LIMITS.items():
                if v + 0.5 <= row['Analiza chemiczna katod ANALIZA {} [ppm]'.format(k)]:
                    cond = False
            cycles.append(
                {
                    'params': (row['NUMER GRUPY'], row['DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd'],
                               row['DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']),
                    'percent': row['Klasyfikacja jakościowa katod HMG-S [stosy]'] / row['Klasyfikacja wszystkie'],
                    'match_cond': cond,
                    **get_cycle_info(row['NUMER GRUPY'], row['DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd'],
                                     row['DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd'], data=data, groups_map=groups_map)
                }
            )
    data[name]['Dane produkcji katod'] = data[name]['Dane produkcji katod'][
        ~data[name]['Dane produkcji katod']['NUMER WYTOPU 6 6'].isna()]
    data[name]['Dane produkcji katod']['zrodlo'] = data[name]['Dane produkcji katod']['NUMER WYTOPU 6 6'].apply(
        get_source_wytop)
    liczba_zrodel = data[name]['Dane produkcji katod'][['NUMER GRUPY', 'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
                                                        'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd', 'zrodlo']].groupby(
        ['NUMER GRUPY', 'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
         'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']).nunique().reset_index(drop=False)
    liczba_zrodel['NUMER GRUPY'] = liczba_zrodel['NUMER GRUPY'].astype(int)

    target = pd.DataFrame([[c['percent'], int(c['match_cond'])] for c in cycles], columns=['percent', 'match_cond'])

    tmp_x_1 = pd.DataFrame([c['input_params'][['CYKL']].values[0] for c in cycles], columns=['CYKL'])
    tmp_x_2 = pd.DataFrame([c['input_params'][col_chem].max().values for c in cycles],
                           columns=['{} _max'.format(c) for c in col_chem])
    tmp_x_3 = pd.DataFrame([c['input_params'][col_chem].min().values for c in cycles],
                           columns=['{} _min'.format(c) for c in col_chem])
    tmp_x_4 = pd.DataFrame([c['input_params'][col_chem].max().values - \
                            c['input_params'][col_chem].min().values for c in cycles],
                           columns=['{} _range'.format(c) for c in col_chem])
    tmp_x_5 = pd.DataFrame([[c['cycle_run']['Poziom elektrolitu Zbiorniki naporowe'][:4 * 24].max(),
                             c['cycle_run']['Poziom elektrolitu Zbiorniki naporowe'][:4 * 24].mean()] for c in cycles],
                           columns=['Poziom elektrolitu Zbiorniki naporowe _max',
                                    'Poziom elektrolitu Zbiorniki naporowe_mean'])
    tmp_x_6 = pd.DataFrame([[c['cycle_run']['Poziom elektrolitu Zbiorniki cyrkulacyjne'][:4 * 24].min(),
                             c['cycle_run']['Poziom elektrolitu Zbiorniki cyrkulacyjne'][:4 * 24].max() -
                             c['cycle_run']['Poziom elektrolitu Zbiorniki cyrkulacyjne'][:4 * 24].min()] for c in
                            cycles],
                           columns=['Poziom elektrolitu Zbiorniki cyrkulacyjne _min',
                                    'Poziom elektrolitu Zbiorniki cyrkulacyjne _range'])
    tmp_x_7 = pd.DataFrame([[c['cycle_run']['Temperatura Zbiorniki naporowe'][:4 * 24].mean(),
                             c['cycle_run']['Temperatura Zbiorniki cyrkulacyjne'][:4 * 24].max() - \
                             c['cycle_run']['Temperatura Zbiorniki cyrkulacyjne'][:4 * 24].min()]
                            for c in cycles],
                           columns=['Temperatura Zbiorniki naporowe_mean',
                                    'Temperatura Zbiorniki cyrkulacyjne _range'])
    tmp_x_8 = pd.DataFrame([[c['cycle_run']['NaCl_przepływ'][:4 * 24].quantile(0.25),
                             c['cycle_run']['Kondensat [m3]'][:4 * 24].sum(),
                             c['cycle_run']['NaCl [kg]'][:4 * 24].max() - \
                             c['cycle_run']['NaCl [kg]'][:4 * 24].min()]
                            for c in cycles],
                           columns=['NaCl_przepływ _q25',
                                    'Kondensat [m3] _sum',
                                    'NaCl [kg] _range'])
    tmp_x_9 = pd.DataFrame([[c['cycle_run']['H2SO4'][:4 * 24].min(),
                             c['cycle_run']['H2SO4'][:4 * 24].mean(),
                             c['cycle_run']['H2SO4'][:4 * 24].max() - \
                             c['cycle_run']['H2SO4'][:4 * 24].min()]
                            for c in cycles],
                           columns=['H2SO4 _min',
                                    'H2SO4 _mean',
                                    'H2SO4 _range'])
    tmp_x_10 = pd.DataFrame([[c['cycle_run']['Cu'][:4 * 24].min(),
                              c['cycle_run']['Cu'][:4 * 24].max() - \
                              c['cycle_run']['Cu'][:4 * 24].min(),
                              c['cycle_run']['Cl'][:4 * 24].max(),
                              c['cycle_run']['Cl'][:4 * 24].mean(),
                              c['cycle_run']['Cl'][:4 * 24].max() - \
                              c['cycle_run']['Cl'][:4 * 24].min()
                              ] for c in cycles],
                            columns=['Cu _min',
                                     'Cu _range',
                                     'Cl _max',
                                     'Cl _mean',
                                     'Cl _range'])
    tmp_x_11 = pd.DataFrame([[kiedy_lewarowanie(c['cycle_run'][:4 * 24]),
                              ] for c in cycles],
                            columns=['kiedy_lewarowanie'])
    tmp_x_12 = pd.DataFrame([[c['cycle_run']['Tiomocznik_przepływ'][:4 * 24].sum(),
                              piki(c['cycle_run'][['Tiomocznik_przepływ'][:4 * 24]]),
                              pik_dol(c['cycle_run'][['Tiomocznik_przepływ']][:4 * 24])
                              ] for c in cycles],
                            columns=['Tiomocznik_przepływ _sum',
                                     'piki',
                                     'pik_dol'])
    tmp_x_13 = pd.DataFrame([[c['cycle_run']['Ni'][:4 * 24].max(),
                              c['cycle_run']['Ni'][:4].mean(),
                              c['cycle_run']['Bi'][:4 * 24].min(),
                              c['cycle_run']['As'][:4 * 24].min(),
                              c['cycle_run']['Napięcie'][:4 * 24].quantile(0.75),
                              c['cycle_run']['Napięcie'][:4 * 24].mean()
                              ] for c in cycles],
                            columns=['Ni _max',
                                     'Ni _open',
                                     'Bi _min',
                                     'As _min',
                                     'Napięcie _q75',
                                     'Napięcie_mean'])

    input_data = pd.concat([tmp_x_1, tmp_x_2, tmp_x_3, tmp_x_4, tmp_x_5, tmp_x_6, tmp_x_7, tmp_x_8, tmp_x_9, tmp_x_10,
                            liczba_zrodel, tmp_x_11, tmp_x_12, tmp_x_13], axis=1)
    input_data = input_data[~input_data['CYKL'].isna()]
    input_data = input_data.fillna(value=input_data.mean())
    input_data['obieg_cyrkulacyjny'] = input_data['NUMER GRUPY'].apply(obiegi_mapper)
    input_data.drop(columns=['NUMER GRUPY',
                             'DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd',
                             'DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd'], inplace=True, errors='ignore')
    input_data.columns = [c.replace(',', '') for c in input_data.columns]
    input_data.columns = [c.replace('[', '') for c in input_data.columns]
    input_data.columns = [c.replace(']', '') for c in input_data.columns]
    input_data = input_data[['CYKL',
                             'Analiza chemiczna anod  ANALIZA Pb % _max',
                             'Analiza chemiczna anod  ANALIZA As % _max',
                             'Analiza chemiczna anod  ANALIZA Ni % _max',
                             'Analiza chemiczna anod  ANALIZA S % _max',
                             'Analiza chemiczna anod  ANALIZA Sb % _max',
                             'Analiza chemiczna anod  ANALIZA O2 % _max',
                             'Analiza chemiczna anod  ANALIZA Bi % _max',
                             'Analiza chemiczna anod  ANALIZA Sn % _max',
                             'Analiza chemiczna anod  ANALIZAAg % _max',
                             'Analiza chemiczna anod  ANALIZA Pb % _min',
                             'Analiza chemiczna anod  ANALIZA As % _min',
                             'Analiza chemiczna anod  ANALIZA Ni % _min',
                             'Analiza chemiczna anod  ANALIZA S % _min',
                             'Analiza chemiczna anod  ANALIZA Sb % _min',
                             'Analiza chemiczna anod  ANALIZA O2 % _min',
                             'Analiza chemiczna anod  ANALIZA Bi % _min',
                             'Analiza chemiczna anod  ANALIZA Sn % _min',
                             'Analiza chemiczna anod  ANALIZAAg % _min',
                             'Analiza chemiczna anod  ANALIZA Pb % _range',
                             'Analiza chemiczna anod  ANALIZA As % _range',
                             'Analiza chemiczna anod  ANALIZA Ni % _range',
                             'Analiza chemiczna anod  ANALIZA S % _range',
                             'Analiza chemiczna anod  ANALIZA Sb % _range',
                             'Analiza chemiczna anod  ANALIZA O2 % _range',
                             'Analiza chemiczna anod  ANALIZA Bi % _range',
                             'Analiza chemiczna anod  ANALIZA Sn % _range',
                             'Analiza chemiczna anod  ANALIZAAg % _range',
                             'Poziom elektrolitu Zbiorniki naporowe _max',
                             'Poziom elektrolitu Zbiorniki naporowe_mean',
                             'Poziom elektrolitu Zbiorniki cyrkulacyjne _min',
                             'Poziom elektrolitu Zbiorniki cyrkulacyjne _range',
                             'Temperatura Zbiorniki naporowe_mean',
                             'Temperatura Zbiorniki cyrkulacyjne _range',
                             'NaCl_przepływ _q25',
                             'Kondensat m3 _sum',
                             'NaCl kg _range',
                             'H2SO4 _min',
                             'H2SO4 _mean',
                             'H2SO4 _range',
                             'Cu _min',
                             'Cu _range',
                             'Cl _max',
                             'Cl _mean',
                             'Cl _range',
                             'zrodlo',
                             'obieg_cyrkulacyjny',
                             'kiedy_lewarowanie',
                             'Tiomocznik_przepływ _sum',
                             'piki',
                             'pik_dol']]
    input_data['CYKL'] = input_data['CYKL'].astype(int)
    input_data['zrodlo'] = input_data['zrodlo'].astype(int)
    input_data['obieg_cyrkulacyjny'] = input_data['obieg_cyrkulacyjny'].astype(int)
    input_data['kiedy_lewarowanie'] = input_data['kiedy_lewarowanie'].astype(int)
    return input_data, (target['percent'] > 0.9999).astype(int)
