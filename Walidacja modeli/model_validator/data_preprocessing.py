from model_validator.data_preprocessing_utils import prepare_data_model_miedzi_Cu_CATH1_NSP, \
    prepare_data_model_miedzi_bialej_pelen_cykl, \
    prepare_data_model_miedzi_bialej_po_4_dniach

preprocessors = {
    'model_miedzi_Cu_CATH1_NSP': prepare_data_model_miedzi_Cu_CATH1_NSP,
    'model_miedzi_białej_pełen_cykl': prepare_data_model_miedzi_bialej_pelen_cykl,
    'model_miedzi_białej_po_4_dniach': prepare_data_model_miedzi_bialej_po_4_dniach
}


def get_preprocess(model: str):
    return preprocessors.get(model)


def prepare_data(model, location='data'):
    preprocessor = get_preprocess(model)
    data, target = preprocessor(location)
    return data, target
