import os
import pickle5 as pickle
import warnings
from typing import Union, Optional, Dict

import matplotlib.pyplot as plt
import pandas as pd
import plotly.express as px
from catboost import CatBoostClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, balanced_accuracy_score, recall_score, precision_score, roc_curve, auc

warnings.simplefilter(action='ignore', category=FutureWarning)

models = {
    'model_miedzi_Cu_CATH1_NSP': 'Cu_CATH1_NSP.cbm',
    'model_miedzi_białej_pełen_cykl': 'rf_model.pickle',
    'model_miedzi_białej_po_4_dniach': 'model_pierwsze_4_dni.cbm'
}


def load_model(model: str, location: str = 'model_validator/models') -> Union[
    CatBoostClassifier, RandomForestClassifier]:
    """
    Function to load model
    :param model: name of the model
    :param location: filepath to model's repository location
    :return: selected model
    """
    if model == 'model_miedzi_Cu_CATH1_NSP' or model == 'model_miedzi_białej_po_4_dniach':
        return CatBoostClassifier().load_model(os.path.join(location, models.get(model)))
    elif model == 'model_miedzi_białej_pełen_cykl':
        with open(os.path.join(location, models.get(model)), 'rb') as f:
            return pickle.load(f)
    else:
        raise NameError(f'Unknown model, available models are {list(models.keys())}, but {model} was specified')


def show_feature_importance(model: Union[CatBoostClassifier, RandomForestClassifier], top: int = 10,
                            data: Optional[pd.DataFrame] = None):
    """
    Function to extract and show feature importance for a specific model.
    :param model: instance of a model to analyze
    :param top: how many features to show on the plot
    :param data: data for model prediction, of model is of type CatBoostClassifier this parameter can be skipped
    :return: figure with feature importance for top features
    """
    if isinstance(model, CatBoostClassifier):
        importance = pd.DataFrame({'feature': model.feature_names_,
                                   'importance': model.get_feature_importance()}).sort_values(by='importance',
                                                                                              ascending=False).set_index(
            'feature')
    else:
        assert data is not None, 'Please provide data for model testing!'
        importance = pd.DataFrame({'feature': list(data.columns),
                                   'importance': model.best_estimator_.feature_importances_}).sort_values(
            by='importance', ascending=False).set_index(
            'feature')
    fig = px.bar(importance.head(top), y='importance')
    fig.show()


def score_model(model: Union[CatBoostClassifier, RandomForestClassifier], x_test: pd.DataFrame,
                y_test: Union[pd.DataFrame, pd.Series]) -> Dict:
    """
    Score classification model
    :param model: instance of a model to analyze
    :param x_test: input data for prediction
    :param y_test: ground truth labels
    :return: dictionary with AUC, balanced accuracy, recall, precision and confusion matrix
    """
    result = {}
    prediction = model.predict_proba(x_test)[:, 1]
    result['AUC'] = roc_auc_score(y_test, prediction)
    prediction = model.predict(x_test)
    result['balanced_accuracy'] = balanced_accuracy_score(y_test, prediction)
    result['recall'] = recall_score(y_test, prediction)
    result['precision'] = precision_score(y_test, prediction)
    return result


def plot_cm(model: Union[CatBoostClassifier, RandomForestClassifier], x_test: pd.DataFrame,
            y_test: Union[pd.DataFrame, pd.Series]):
    """
    Calculate and plot the confusion matrix
    :param model: instance of a model to analyze
    :param x_test: input data for prediction
    :param y_test: ground truth labels
    """
    from sklearn.metrics import plot_confusion_matrix
    plot_confusion_matrix(model, x_test, y_test)
    plt.show()


def plot_roc_curve(model: Union[CatBoostClassifier, RandomForestClassifier], x_test: pd.DataFrame,
                   y_test: Union[pd.DataFrame, pd.Series]):
    """
    Plot ROC curve for analyzed model
    :param model: instance of a model to analyze
    :param x_test: input data for prediction
    :param y_test: ground truth labels
    """
    fpr, tpr, _ = roc_curve(y_test, model.predict_proba(x_test)[:, 1])
    roc_auc = auc(fpr, tpr)
    plt.figure()
    lw = 2
    plt.plot(
        fpr,
        tpr,
        color="darkorange",
        lw=lw,
        label="ROC curve (area = %0.2f)" % roc_auc,
    )
    plt.plot([0, 1], [0, 1], color="navy", lw=lw, linestyle="--")
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title("Receiver operating characteristic example")
    plt.legend(loc="lower right")
    plt.show()


def print_metrics(model: Union[CatBoostClassifier, RandomForestClassifier], x_test: pd.DataFrame,
                  y_test: Union[pd.DataFrame, pd.Series]):
    """
    Score and print the metrics
    :param model: instance of a model to analyze
    :param x_test: input data for prediction
    :param y_test: ground truth labels
    """
    result = score_model(model, x_test, y_test)
    for key, value in result.items():
        print(f'{key}: {round(value, 3)}\n')


def validate_model(model: Union[CatBoostClassifier, RandomForestClassifier], x_test: pd.DataFrame,
                   y_test: Union[pd.DataFrame, pd.Series]):
    """
    Summary function for model validation
    :param model: instance of a model to analyze
    :param x_test: input data for prediction
    :param y_test: ground truth labels
    """
    print_metrics(model, x_test, y_test)
    plot_roc_curve(model, x_test, y_test)
    plot_cm(model, x_test, y_test)
