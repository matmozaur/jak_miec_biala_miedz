from dash import html
import dash_bootstrap_components as dbc


card_visualization = dbc.Card(
    [
        dbc.CardBody(
            [
                html.H4("Wizualizacja danych", className="card-title"),
                html.P(
                    '''
                    Wizualizacja otrzymanych danych \n \n
                    ''',
                    className="card-text",
                    style={"height": "9rem"}

                ),
                dbc.Button("Odwiedź", href="/data_visualization", color="primary"),
            ]
        ),
    ],
    style={"width": "21rem", "margin-left": "10px", "margin-right": "10px"},
    className="h-100 p-3 bg-light border rounded-1"
)

card_prediction = dbc.Card(
    [
        dbc.CardBody(
            [
                html.H4("Predykcja jakości katod", className="card-title"),
                html.P(
                   '''
                   Predykcja jakości katod na podstawie pełnego przebiegu procesu
                   ''',
                    className="card-text",
                    style={"height": "7rem"}
                ),
                dbc.Button("Odwiedź",  href="/full_process_results_prediction",color="primary"),
            ],

        ),
    ],
    style={"width": "21rem",  "margin-left": "10px", "margin-right": "10px"},
    className="h-100 p-3 bg-light border rounded-1"
)

card_process_pred = dbc.Card(
    [
        dbc.CardBody(
            [
                html.H4("Predykcja parametrów procesu", className="card-title"),
                html.P(
                    '''
                    Określenie optymalnych parametrów procesu dla podanych anod
                    ''',
                    className="card-text",
                    style={"height": "7rem"}

                ),
                dbc.Button("Odwiedź", href="/process_parameters_prediction", color="primary"),
            ]
        ),
    ],
    style={"width": "21rem", "margin-left": "10px", "margin-right": "10px"},
    className="h-100 p-3 bg-light border rounded-1"
)


card_container = dbc.Row([card_visualization, card_prediction, card_process_pred], className="jumbotron")





def create_home():
    subpage = card_container

    return subpage

