import dash
import dash_bootstrap_components as dbc
from dash import Input, Output, dcc, html
from home import create_home
from navbar import create_navbar
from data_visualizer import create_visualizer
from predictor import create_predictor
from parameters_predictor import create_parameters_predictor

app = dash.Dash(external_stylesheets=[dbc.themes.MINTY])


# the style arguments for the sidebar. We use position:fixed and a fixed width
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

# the styles for the main content position it to the right of the sidebar and
# add some padding.
CONTENT_STYLE = {
    "margin-left": "15rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

SUBCONTENT_STYLE = {
    "margin-left": "4rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}


sidebar = html.Div(
    [
        html.H2("Menu", className="display-4"),
        html.Hr(),
        dbc.Nav(
            [
                dbc.NavLink("Home", href="/", active="exact"),
                dbc.NavLink("Wizualizacja Danych", href="/data_visualization", active="exact"),
                dbc.NavLink("Predykcja jakości katod", href="/full_process_results_prediction", active="exact"),
                dbc.NavLink("Predykcja parametrów procesu", href="/process_parameters_prediction", active="exact"),

            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)

navbar = create_navbar()


subpage_content = html.Div(id="subpage-content", style=SUBCONTENT_STYLE)

content = html.Div(id="page-content", style=CONTENT_STYLE, children=[navbar,subpage_content])


app.layout = html.Div([dcc.Location(id="url"), sidebar, content])


@app.callback(Output("subpage-content", "children"), [Input("url", "pathname")])
def render_page_content(pathname):
    if pathname == "/":
        return create_home()
    elif pathname == "/data_visualization":
        return create_visualizer()
    elif pathname == "/full_process_results_prediction":
        return create_predictor()
    elif pathname == "/process_parameters_prediction":
        return create_parameters_predictor()


if __name__ == "__main__":
    app.run_server(port=5567, debug=False)