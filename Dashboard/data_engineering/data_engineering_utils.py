import pandas as pd


def leverage_time(data):
    elektrolit = data[['Elektrolit [m3]']].dropna()
    elektrolit = elektrolit.reset_index(drop=True).reset_index(drop=False)
    elektrolit = elektrolit[elektrolit['Elektrolit [m3]'] > 0]
    try:
        return elektrolit['index'].iloc[0]
    except IndexError:
        return -1


def spikes(tiomocznik_df):
    tiomocznik_df['diff'] = tiomocznik_df['Tiomocznik_przepływ'].diff()
    wzrosty = tiomocznik_df[tiomocznik_df['diff'] > 2].shape[0]
    spadki = tiomocznik_df[tiomocznik_df['diff'] < -2].shape[0]
    return (wzrosty + spadki) // 2


def down_spikes(tiomocznik_df):
    tiomocznik_mean = tiomocznik_df['Tiomocznik_przepływ'].mean()
    tiomocznik_df['diff'] = tiomocznik_df['Tiomocznik_przepływ'].diff()
    return tiomocznik_df[(tiomocznik_df['diff'] < -2) & \
                         (tiomocznik_df['Tiomocznik_przepływ'] < (tiomocznik_mean - 2))].shape[0]


def update_by_anods(c, tmp_x):
    col_chem = ['Analiza chemiczna anod  ANALIZA Pb [%]',
                'Analiza chemiczna anod  ANALIZA As [%]',
                'Analiza chemiczna anod  ANALIZA Ni [%]',
                'Analiza chemiczna anod  ANALIZA S [%]',
                'Analiza chemiczna anod  ANALIZA Sb [%]',
                'Analiza chemiczna anod  ANALIZA O2 [%]',
                'Analiza chemiczna anod  ANALIZA Bi [%]',
                'Analiza chemiczna anod  ANALIZA Sn [%]',
                'Analiza chemiczna anod  ANALIZA Zn [%]',
                'Analiza chemiczna anod  ANALIZAAg [%]']

    tmp_x_2 = pd.DataFrame([c['input_params'][col_chem].max().values],
                           columns=['{} _max'.format(c) for c in col_chem])
    tmp_x_3 = pd.DataFrame([c['input_params'][col_chem].min().values],
                           columns=['{} _min'.format(c) for c in col_chem])
    tmp_x_4 = pd.DataFrame([c['input_params'][col_chem].max().values - \
                            c['input_params'][col_chem].min().values],
                           columns=['{} _range'.format(c) for c in col_chem])

    return pd.concat([tmp_x, tmp_x_2, tmp_x_3, tmp_x_4], axis=1)


def update_by_containers(c, tmp_x):
    tmp_x_5 = pd.DataFrame([[c['cycle_run']['Poziom elektrolitu Zbiorniki naporowe'].max(),
                             c['cycle_run']['Poziom elektrolitu Zbiorniki naporowe'].mean(),
                             c['cycle_run']['Poziom elektrolitu Zbiorniki naporowe'][4 * 24:-4 * 24].mean(),
                             c['cycle_run']['Poziom elektrolitu Zbiorniki naporowe'][:-4 * 24].mean()]],
                           columns=['Poziom elektrolitu Zbiorniki naporowe _max',
                                    'Poziom elektrolitu Zbiorniki naporowe_mean',
                                    'Poziom elektrolitu Zbiorniki naporowe _ph2_mean',
                                    'Poziom elektrolitu Zbiorniki naporowe _ph1_2_mean'])

    tmp_x_6 = pd.DataFrame([[c['cycle_run']['Poziom elektrolitu Zbiorniki cyrkulacyjne'].min(),
                             c['cycle_run']['Poziom elektrolitu Zbiorniki cyrkulacyjne'].max() -
                             c['cycle_run']['Poziom elektrolitu Zbiorniki cyrkulacyjne'].min()]],
                           columns=['Poziom elektrolitu Zbiorniki cyrkulacyjne _min',
                                    'Poziom elektrolitu Zbiorniki cyrkulacyjne _range'])

    tmp_x_7 = pd.DataFrame([[c['cycle_run']['Temperatura Zbiorniki naporowe'][4 * 24:-4 * 24].mean(),
                             c['cycle_run']['Temperatura Zbiorniki naporowe'].mean(),
                             c['cycle_run']['Temperatura Zbiorniki cyrkulacyjne'].max() - \
                             c['cycle_run']['Temperatura Zbiorniki cyrkulacyjne'].min()]
                            ],
                           columns=['Temperatura Zbiorniki naporowe _ph2_mean',
                                    'Temperatura Zbiorniki naporowe_mean',
                                    'Temperatura Zbiorniki cyrkulacyjne _range'])

    return pd.concat([tmp_x, tmp_x_5, tmp_x_6, tmp_x_7], axis=1)


def update_by_condensate(c, tmp_x):
    tmp_x_8 = pd.DataFrame([[c['cycle_run']['NaCl_przepływ'].quantile(0.25),
                             c['cycle_run']['Kondensat [m3]'].sum(),
                             c['cycle_run']['NaCl [kg]'].max() - \
                             c['cycle_run']['NaCl [kg]'].min()]
                            ],
                           columns=['NaCl_przepływ _q25',
                                    'Kondensat [m3] _sum',
                                    'NaCl [kg] _range'])

    tmp_x_9 = pd.DataFrame([[c['cycle_run']['H2SO4'].min(),
                             c['cycle_run']['H2SO4'].mean(),
                             c['cycle_run']['H2SO4'].max() - \
                             c['cycle_run']['H2SO4'].min(),
                             c['cycle_run']['H2SO4'][4 * 24:-4 * 24].mean(),
                             c['cycle_run']['H2SO4'][:-4 * 24].mean(),
                             ]
                            ],
                           columns=['H2SO4 _min',
                                    'H2SO4 _mean',
                                    'H2SO4 _range',
                                    'H2SO4 _phase2_mean',
                                    'H2SO4 _phase1_2_mean'])

    tmp_x_10 = pd.DataFrame([[c['cycle_run']['Cu'].min(),
                              c['cycle_run']['Cu'].max() - \
                              c['cycle_run']['Cu'].min(),
                              c['cycle_run']['Cl'].max(),
                              c['cycle_run']['Cl'].mean(),
                              c['cycle_run']['Cl'].max() - \
                              c['cycle_run']['Cl'].min(),
                              c['cycle_run']['Cl'][4 * 24:-4 * 24].mean(),
                              c['cycle_run']['Cl'][:-4 * 24].mean(),
                              ]
                             ],
                            columns=['Cu _min',
                                     'Cu _range',
                                     'Cl _max',
                                     'Cl _mean',
                                     'Cl _range',
                                     'Cl _phase2_mean',
                                     'Cl _phase1_2_mean'])

    return pd.concat([tmp_x, tmp_x_8, tmp_x_9, tmp_x_10], axis=1)


def update_by_leverage_and_spikes(c, tmp_x):
    tmp_x_11 = pd.DataFrame([[leverage_time(c['cycle_run']),
                              ]
                             ],
                            columns=['kiedy_lewarowanie',
                                     ])

    tmp_x_12 = pd.DataFrame([[c['cycle_run']['Tiomocznik_przepływ'].sum(),
                              spikes(c['cycle_run'][['Tiomocznik_przepływ']]),
                              down_spikes(c['cycle_run'][['Tiomocznik_przepływ']])
                              ]
                             ],
                            columns=['Tiomocznik_przepływ _sum',
                                     'piki',
                                     'pik_dol'
                                     ])

    return pd.concat([tmp_x, tmp_x_11, tmp_x_12], axis=1)


def update_by_power(c, tmp_x):
    tmp_x_13 = pd.DataFrame([[c['cycle_run']['Ni'][0],
                              c['cycle_run']['Ni'].max(),
                              c['cycle_run']['Bi'][-1],
                              c['cycle_run']['Bi'][-4 * 24:].min(),
                              c['cycle_run']['Bi'].min(),
                              c['cycle_run']['As'].min(),
                              ]
                             ],
                            columns=['Ni _open',
                                     'Ni _max',
                                     'Bi _close',
                                     'Bi _phase3_mean',
                                     'Bi _min',
                                     'As _min'])

    tmp_x_14 = pd.DataFrame([[c['cycle_run']['Napięcie'][-4 * 24:].mean(),
                              c['cycle_run']['Napięcie'][-1],
                              c['cycle_run']['Napięcie'].quantile(0.75),
                              c['cycle_run']['Napięcie'].mean(),
                              ]
                             ],
                            columns=['Napięcie _phase3_mean',
                                     'Napięcie _close',
                                     'Napięcie _q75',
                                     'Napięcie _mean'])

    return pd.concat([tmp_x, tmp_x_13, tmp_x_14], axis=1)

