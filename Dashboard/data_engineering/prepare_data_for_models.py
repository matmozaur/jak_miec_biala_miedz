import pandas as pd
from .data_engineering_utils import *


def prepare_for_main_model(cycles, group, start):
    c = cycles[group][start]
    tmp_y = pd.DataFrame([[c['percent'], int(c['match_cond'])]], columns=['percent', 'match_cond'])
    tmp_x = pd.DataFrame([c['input_params'][['CYKL']].values[0]], columns=['CYKL'])

    tmp_x = update_by_anods(c, tmp_x)
    tmp_x = update_by_containers(c, tmp_x)
    tmp_x = update_by_condensate(c, tmp_x)
    tmp_x = update_by_leverage_and_spikes(c, tmp_x)
    tmp_x = update_by_power(c, tmp_x)

    return tmp_x, tmp_y


