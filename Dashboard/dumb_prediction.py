import pickle
import pandas as pd
from loading_data.cache_data import load_data
from data_engineering.prepare_data_for_models import prepare_for_main_model


with open('models/rf_model_main.pickle', 'rb') as handle:
    model = pickle.load(handle)
cycles = load_data()
x, y = prepare_for_main_model(cycles, 2, pd.Timestamp('2021-03-03 23:00:50'))
with open('models/rf_model_sc.pickle', 'rb') as handle:
    sc = pickle.load(handle)
x = sc.transform([x.loc[0]])
print(model.predict_proba(x)[0, 1])
print(y['percent'][0])
