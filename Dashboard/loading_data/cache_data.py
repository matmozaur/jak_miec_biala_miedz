from .utils_group_cycles import *


def load_data():
    data, groups_map = prepare_all_data('data', fill=False)
    for k, v in groups_map.items():
        v['data'].columns = [re.sub('Obwód \d', '', c) for c in v['data'].columns]

    with open('loading_data/file_names.json') as f:
        FILE_NAMES = json.load(f)
    name = FILE_NAMES['file_8_9_11']
    LIMITS = {
        'Ag': 10,
        'Pb': 1,
        'Fe': 2,
        'Ni': 2,
        'S': 4,
        'Zn': 1,
    }
    data[name]['Grupy produkcyjne']['Klasyfikacja wszystkie'] = \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod HMG-S [stosy]'] + \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod Cu- CATH 1 Z [stosy]'] + \
        data[name]['Grupy produkcyjne']['Klasyfikacja jakościowa katod Cu-CATH 2 [stosy]']

    cycles = {}
    for i, row in data[name]['Grupy produkcyjne'].iterrows():
        if row['Klasyfikacja wszystkie'] > 0 and row['TRYB'] == 'PROD':
            cond = True
            for k, v in LIMITS.items():
                if v + 0.5 <= row['Analiza chemiczna katod ANALIZA {} [ppm]'.format(k)]:
                    cond = False
            if row['NUMER GRUPY'] not in cycles.keys():
                cycles[row['NUMER GRUPY']] = {}
            cycles[row['NUMER GRUPY']][row['DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd']] = {
                    'params': (row['NUMER GRUPY'], row['DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd'],
                               row['DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd']),
                    'percent': row['Klasyfikacja jakościowa katod HMG-S [stosy]'] / row['Klasyfikacja wszystkie'],
                    'match_cond': cond,
                    **get_cycle_info(row['NUMER GRUPY'], row['DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd'],
                                     row['DATA WYŁĄCZENIA GRUPY  rrrr-mm-dd'], data=data, groups_map=groups_map)
                }
    return cycles
