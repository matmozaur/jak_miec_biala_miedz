import pandas as pd
from .utils_circulations import *
import re
import os

IN_COLS = ['TRYB', 'CYKL', 'NUMER WYTOPU 6 6',
           'OPIS WYTOPU 7 7', 'ZRÓDLO WYTOPU 8 8',
           'Analiza chemiczna anod  ANALIZA Pb [%]',
           'Analiza chemiczna anod  ANALIZA As [%]',
           'Analiza chemiczna anod  ANALIZA Ni [%]',
           'Analiza chemiczna anod  ANALIZA S [%]',
           'Analiza chemiczna anod  ANALIZA Sb [%]',
           'Analiza chemiczna anod  ANALIZA O2 [%]',
           'Analiza chemiczna anod  ANALIZA Bi [%]',
           'Analiza chemiczna anod  ANALIZA Sn [%]',
           'Analiza chemiczna anod  ANALIZA Zn [%]',
           'Analiza chemiczna anod  ANALIZAAg [%]', 'MASA WYTOPU 9 [Mg]',
           'ENERGIA 0 [kWh]', 'WYDAJNOŚĆ 1 [%]']

OUT_COLS = ['MASA GRUPY  [kg]', 'Klasyfikacja jakościowa katod HMG-S [stosy]',
            'Klasyfikacja jakościowa katod Cu- CATH 1 Z [stosy]',
            'Klasyfikacja jakościowa katod Cu-CATH 2 [stosy]',
            'Klasyfikacja jakościowa katod Katody niesklasyfikowane [sztuki]',
            'Analiza chemiczna katod ANALIZA Pb [ppm]',
            'Analiza chemiczna katod ANALIZA Fe [ppm]',
            'Analiza chemiczna katod ANALIZA Ni [ppm]',
            'Analiza chemiczna katod ANALIZA Zn [ppm]',
            'Analiza chemiczna katod ANALIZA S [ppm]',
            'Analiza chemiczna katod ANALIZA Ag [ppm]']


def prepare_groups(directory):
    data = {}

    for filename in os.listdir(directory):
        if '.xlsx' in filename:
            data[filename] = pd.read_excel('{}/{}'.format(directory, filename), sheet_name=None)

    obiegi = prepare_circulation(data)
    obwody = prepare_circuits(data)

    groups_map = {
        k: {
            'obieg': (k - 1) // 8 + 1,
            'obwód': (k - 1) // 16 + 1
        } for k in range(1, 97)
    }
    groups_map[1]['obieg'] = 2
    groups_map[2]['obieg'] = 2

    for k, v in groups_map.items():
        v['data'] = pd.concat([obiegi[v['obieg']], obwody[v['obwód']]], axis=1)

    name = FILE_NAMES['file_8_9_11']

    data[name] = pd.read_excel('data/{}'.format(name), sheet_name=None, header=[0, 1, 2])

    data[name]['Grupy produkcyjne'].columns = \
        data[name]['Grupy produkcyjne'].columns.map(lambda h: '{} {} {}'.format(h[0], h[1], h[2]))

    data[name]['Grupy produkcyjne'].columns = \
        data[name]['Grupy produkcyjne'].columns.map(lambda h: re.sub('Unnamed: \d', '', h))

    data[name]['Grupy produkcyjne'].columns = \
        data[name]['Grupy produkcyjne'].columns.map(lambda h: re.sub('_level_\d', '', h))

    data[name]['Grupy produkcyjne'].columns = \
        data[name]['Grupy produkcyjne'].columns.map(lambda h: h.strip())

    data[name]['Dane produkcji katod'].columns = \
        data[name]['Dane produkcji katod'].columns.map(lambda h: '{} {} {}'.format(h[0], h[1], h[2]))

    data[name]['Dane produkcji katod'].columns = \
        data[name]['Dane produkcji katod'].columns.map(lambda h: re.sub('Unnamed: \d', '', h))

    data[name]['Dane produkcji katod'].columns = \
        data[name]['Dane produkcji katod'].columns.map(lambda h: re.sub('_level_\d', '', h))

    data[name]['Dane produkcji katod'].columns = \
        data[name]['Dane produkcji katod'].columns.map(lambda h: h.strip())

    data[name]['Dane produkcji katod'] = data[name]['Dane produkcji katod'].ffill()
    return data, groups_map


def prepare_6(data, groups_map):
    name = FILE_NAMES['file_6']

    for k in data[name].keys():
        tmp = data[name][k]
        tmp['Data'] = pd.to_datetime(tmp['Data'])
        tmp.set_index('Data', inplace=True)
        data[name][k] = tmp

    for k in range(1, 97):
        groups_map[k]['data']['Napięcie'] = data[name]['Napiecie na grupach wanien'][['Grupa {}'.format(k)]]


def prepare_7(data, groups_map):
    name = FILE_NAMES['file_7']
    for k in data[name].keys():
        tmp = data[name][k]
        tmp['Data'] = pd.to_datetime(tmp['Data'])
        tmp.set_index('Data', inplace=True)
        data[name][k] = tmp

    for k in range(1, 97):
        groups_map[k]['data'] = pd.concat([groups_map[k]['data'],
                                           data[name]['grupy'][data[name]['grupy']['Grupa'] == k].drop('Grupa',
                                                                                                       axis=1)], axis=1)


def prepare_all_data(directory, fill=False):
    data, groups_map = prepare_groups(directory)
    if fill:
        for k, v in groups_map.items():
            v['data'] = v['data'].ffill()
    prepare_6(data, groups_map)
    prepare_7(data, groups_map)
    return data, groups_map


def get_cycle_info(group, start, end, data, groups_map):
    name = FILE_NAMES['file_8_9_11']
    relevant = data[name]['Dane produkcji katod'].copy()
    relevant = relevant[(relevant['NUMER GRUPY'] == group) & \
                        (relevant['DATA ZAŁĄCZENIA GRUPY  rrrr-mm-dd'] == start)]
    return {
        'cycle_run': groups_map[group]['data'][start:end],
        'input_params': relevant[IN_COLS],
        'output_params': relevant[OUT_COLS],
    }
