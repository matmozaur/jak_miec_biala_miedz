import dash_bootstrap_components as dbc


def create_navbar():
    navbar = dbc.NavbarSimple(
        brand="RS-TEAM",
        color="primary",
        dark=True,
    )
    return navbar
