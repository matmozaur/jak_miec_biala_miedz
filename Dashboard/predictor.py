from dash import dcc, dash_table
import dash_bootstrap_components as dbc
from dash import State
from file_uploader import *
from loading_data.cache_data import load_data
import pickle
from data_engineering.prepare_data_for_models import prepare_for_main_model
import pandas as pd

loaded_data = False

set_files = False
if not loaded_data:
    CYCLES = load_data()
loaded_data = True

model = 1
to_predict = [(1, pd.Timestamp('2021-03-23T11:48:56')), (1, pd.Timestamp('2021-04-05T20:58:45'))]
predictions = []


def create_pred_table(pred_dict):
    df = pd.DataFrame(pred_dict)
    df = df[["numer grupy","czas rozpoczęcia", "prawd. 100% białych","% poprawnych stosów", "spełnione warunki na Cu-CATH-1 NSP1"]]
    print(df.to_dict())
    table = dash_table.DataTable(df.to_dict('records'), [{"name": i, "id": i} for i in df.columns])
    return table


def make_prediction(group_num, time):
    with open('models/rf_model_main.pickle', 'rb') as handle:
        model = pickle.load(handle)
    global CYCLES
    x, y = prepare_for_main_model(CYCLES, group_num, time)
    with open('models/rf_model_sc.pickle', 'rb') as handle:
        sc = pickle.load(handle)
    x = sc.transform([x.loc[0]])
    return {"prawd. 100% białych": model.predict_proba(x)[0, 1], "% poprawnych stosów": y['percent'][0], "spełnione warunki na Cu-CATH-1 NSP1": y['match_cond'][0] }


def predict_all():
    print('predict_all')
    global predictions
    global to_predict
    predictions = []
    for group_num, time in to_predict:
        pred = make_prediction(group_num, time)
        pred["numer grupy"] =group_num
        pred["czas rozpoczęcia"] =time
        predictions.append(pred)
    return predictions

def get_default_file_mapping():
    with open('loading_data/file_names_default.json') as f:
        file_mapper = f.read()
    with open('loading_data/file_names.json', 'w') as f:
        f.write(file_mapper)
    return file_mapper


def create_select_cycle_num():
    global CYCLES
    if CYCLES is not None:
        all_options = []
        for num_gr in CYCLES.keys():
            all_options.append({"label": str(num_gr), "value": num_gr})
        return all_options


@callback(Output("placeholder", "is_in"),
          [Input("collapse-button", "n_clicks")])
def data_loader(is_in):
    global loaded_data, set_files
    if not loaded_data and set_files:
        print('yes')
        loaded_data = True
        global CYCLES
        CYCLES = load_data()
    print('nope')


@callback(
    Output("collapse", "is_open"),
    [Input("collapse-button", "n_clicks")],
    [State("collapse", "is_open")],
)
def toggle_collapse(n, is_open):
    if n:
        return not is_open
    return is_open


@callback(
    Output("fade", "is_in"),
    [Input("collapse-button", "n_clicks"), ],
    [State("fade", "is_in")],
)
def toggle_fade(n, is_in):
    if not n:
        return False
    return not is_in


@callback(
    Output("collapse2", "is_open"),
    [Input("make_pred-button", "n_clicks")],
    [State("collapse2", "is_open")],
)
def toggle_collapse(n, is_open):
    if n:
        return not is_open
    return is_open


@callback(
    Output("fade2", "is_in"),
    [Input("make_pred-button", "n_clicks"), ],
    [State("fade2", "is_in")],
)
def toggle_fade(n, is_in):
    if not n:
        return False
    return not is_in


@callback(
    Output('textarea-example-output', 'children'),
    Input('textarea_file_structure', 'value')
)
def update_texta_json_file_structer(value):
    with open('loading_data/file_names.json', 'w') as f:
        f.write(value)
    global set_files
    set_files = True
    return

@callback(
    Output('placeholder', 'children'),
    Input('select_model', 'value')
)
def update_model_to_use(value):
    global model
    model = value
    return

@callback(
    Output('chosen_groups_time', 'children'),
    Input('select_group', 'value')
)
def update_chosen_groups_time(value):
    global CYCLES
    if CYCLES is not None:
        print("update_chosen_groups_time", value)
        checkbox_list = []
        for date in CYCLES[int(value)]:
            checkbox_list.append(dbc.InputGroup([dbc.InputGroupText(dbc.Checkbox()), dbc.Input(value = date)], id=str(date)))
        return checkbox_list

@callback(
    Output(component_id='placeholder', component_property='offset'),
    Input(component_id='xD', component_property='value')
)
def update_output_div(input_value):
    return f'Output: {input_value}'

def create_predictor():
    card_upload = dbc.Card(
        [
            dbc.CardBody(
                [
                    html.H4("Załaduj pliki", className="card-title"),
                    dcc.Upload(
                        id="upload-data",
                        children=html.Div(
                            ["Przeciągnij pliki lub kliknij aby je załadować"]
                        ),
                        style={
                            "width": "100%",
                            "height": "60px",
                            "lineHeight": "60px",
                            "borderWidth": "1px",
                            "borderStyle": "dashed",
                            "borderRadius": "5px",
                            "textAlign": "center",
                            "margin": "10px",
                        },
                        multiple=True,
                    ),
                    html.H5("File List"),
                    html.Ul(id="file-list",
                            style={"height": "50%",
                                   "font-size": "small"}),
                ],
            ),
        ],
        style={"width": "30rem", "height": "33rem", "margin-left": "10px", "margin-right": "10px"},
    )
    card_map = dbc.Card(
        [
            dbc.CardBody(
                [
                    html.H4("Uzupełnij nazwy plików", className="card-title"),

                    dbc.Textarea(
                        value=get_default_file_mapping(),
                        style={"height": "94%", },
                        id='textarea_file_structure',
                    ),
                    html.Div(id='textarea-example-output')

                ],
            ),
        ],
        style={"width": "36rem", "margin-left": "10px", "margin-right": "10px", "height": "33rem"},
    )

    confirmation_button = dbc.Button(
        "Potwierdź",
        id="collapse-button",
        className="mb-3",
        color="primary",
        n_clicks=0,
        style={"margin-top": "10px"}
    )
    make_pred_button = dbc.Button(
        "Zrób predykcję",
        id="make_pred-button",
        className="mb-3",
        color="primary",
        n_clicks=0,
        style={"margin-top": "10px"}
    )

    card_prediction = dbc.Card(
        [
            dbc.CardBody(
                [
                    html.H4("Wynik predykcji:", className="card-title"),
                    dbc.Container([
                        create_pred_table(predict_all())],
                        className="card-text",
                    ),
                ],
            ),
        ],
        style={"width": "90%", "margin-left": "10px", "margin-right": "10px", "height": "33rem"},
    )

    card_setup_prediction = dbc.Card(
        [
            dbc.CardBody(
                [
                    html.H4("Ustawienia predykcji", className="card-title"),
                    dbc.InputGroup(
                        [
                            dbc.Select(
                                options=[
                                    {"label": "Predykcja białej miedzi, cały proces", "value": 1},
                                    {"label": "Predykcja białej miedzi, pierwsze 4 dni procesu", "value": 2},
                                    {"label": "Predykcja miedzi Cu-CATH-1 NSP1", "value": 3},

                                ],
                                id="select_model",
                                value = 1,
                            ),
                            dbc.InputGroupText("Wybierz model"),

                        ],
                        id='select_model_ig',
                    ),
                    html.H4("Wybór cykli", className="card-title"),
                    dbc.InputGroup(
                        [
                            dbc.Select(
                                options=create_select_cycle_num(),
                                id = "select_group",
                                value = 1,
                            ),
                            dbc.InputGroupText("Wybierz grupę"),
                        ],
                    ),
                    html.Div(id='chosen_groups_time'),
                    html.P(id='placeholder'),

                ],
            ),
        ],
        style={"width": "90%", "margin-left": "10px", "margin-right": "10px", "height": "33rem"},
    )

    setup_data = dbc.Collapse(
        dbc.Row(
            [
                card_upload, card_map, confirmation_button]
        ),
        id="collapse",
        is_open=True)

    setup_prediction = dbc.Collapse(dbc.Fade(
        dbc.Row(
            [card_setup_prediction, make_pred_button]
        ),
        id="fade",
        is_in=False,
        appear=False),
        id="collapse2",
        is_open=True
    )

    result_section = dbc.Fade(
        dbc.Row(
            [card_prediction]
        ),
        id="fade2",
        is_in=False,
        appear=True)

    sub_page = html.Div(children=[setup_data, setup_prediction, result_section])

    return sub_page
