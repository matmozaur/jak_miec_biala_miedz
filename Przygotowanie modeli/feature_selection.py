import logging
import time
from functools import wraps
from multiprocessing import cpu_count

import numpy as np
from catboost import CatBoostClassifier
from joblib import Parallel, delayed
from scipy.stats import uniform, randint
from sklearn import feature_selection, model_selection


def timer(function):
    @wraps(function)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = function(*args, **kwargs)
        end = time.time() - start
        print('{} ran in {} s'.format(function.__name__, end))
        return result

    return wrapper


def select_data(selector, data):
    cols = np.array(data.columns)
    cols = cols[selector.get_support()]
    return data[cols]


def get_feature_importance(estimator):
    return estimator.get_feature_importance()


@timer
def variance_selector(data, target=None, threshold=0):
    logging.info('Feature selection based on variance threshold...')
    variance = np.nanvar(data[::600].values, axis=0)
    mask = variance > threshold
    cols = np.array(data.columns)
    cols = cols[mask]
    logging.info(f"Data shape: {data[cols].shape}")
    return data[cols]


# @timer
# def mutual_info_selector(data, target, percentile=75):
#     logging.info('Feature selection based on mutual info...')
#     selector = feature_selection.SelectPercentile(feature_selection.mutual_info_classif, percentile=percentile)
#     selector = selector.fit(data, target.values.ravel())
#     data = select_data(selector, data)
#     logging.info(f"Data shape: {data.shape}")
#     return data


@timer
def parallel_mutual_info_selector(data, target, percentile=75, n_jobs=-1):
    logging.info('Feature selection based on mutual info...')
    logging.info(f'Parallel version is using {cpu_count() if n_jobs < 0 else n_jobs} threads')

    def get_mutual_info(col):
        return feature_selection.mutual_info_classif(data[col].values.reshape(-1, 1), target.values.ravel())[0]

    mutual_info = Parallel(n_jobs=n_jobs)(delayed(get_mutual_info)(col) for col in data.columns)

    support = mutual_info > np.quantile(mutual_info, np.clip(1 - percentile / 100, 0, 1))
    cols = np.array(data.columns)
    cols = cols[support]
    if np.sum(support) == 0:
        return data
    logging.info(f"Data shape: {data[cols].shape}")
    return data[cols]


@timer
def f_score_selector(data, target, percentile=75):
    logging.info('Feature selection based on F-scores...')
    selector = feature_selection.SelectPercentile(feature_selection.f_classif, percentile=percentile)
    selector = selector.fit(data, target.values.ravel())
    data = select_data(selector, data)
    logging.info(f"Data shape: {data.shape}")
    return data


@timer
def rfe_catboost_selector(data, target, task_type='GPU', devices='0', n_features_to_select=300):
    logging.info('Feature selection based on recursive feature elimination...')
    estimator = CatBoostClassifier(silent=True, task_type=task_type, boosting_type='Plain', random_seed=1,
                                   devices=devices, loss_function='MAE')
    selector = feature_selection.RFE(estimator, n_features_to_select=n_features_to_select, step=1,
                                     importance_getter=get_feature_importance)
    selector = selector.fit(data, target.values.ravel())
    data = select_data(selector, data)
    logging.info(f"Data shape: {data.shape}")
    return data


@timer
def rfecv_catboost_selector(data, target, catboost_params, task_type='GPU', devices='0'):
    logging.info('Feature selection based on recursive feature elimination with cross-validation...')
    estimator = CatBoostClassifier(**catboost_params, silent=True, task_type=task_type, boosting_type='Ordered',
                                   bootstrap_type='Bayesian', random_seed=1, devices=devices, loss_function='AUC')
    selector = feature_selection.RFECV(estimator, importance_getter=get_feature_importance, min_features_to_select=30,
                                       cv=model_selection.TimeSeriesSplit(n_splits=3, gap=6), verbose=1)
    selector = selector.fit(data, target.values.ravel())
    data = select_data(selector, data)
    logging.info(f"Data shape: {data.shape}")
    return data


# @timer
# def hyperparameter_optimization(data, target, n_iter=250, task_type="GPU", devices='0'):
#     param_grid = {'iterations': [None, 100, 500, 1000, 2000, 5000],
#                   'learning_rate': [None, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1],
#                   'depth': [None, 2, 4, 6, 8, 10, 12],
#                   'l2_leaf_reg': [1e-4, 1e-3, 1e-2, 1e-1, 1, 2, 3, 4, 5, 10, 15, 100],
#                   'bagging_temperature': [0, 0.1, 0.3, 0.5, 0.8, 1, 1.2, 1.5, 2, 5, 10]}
#     cat = CatBoostClassifier(silent=True, task_type=task_type, boosting_type='Plain', bootstrap_type='Bayesian',
#                              random_seed=1, devices=devices, loss_function='MAE')
#     search = model_selection.RandomizedSearchCV(cat, param_grid, n_iter=n_iter, refit=False,
#                                                 cv=5, verbose=2,
#                                                 scoring='neg_mean_absolute_error', random_state=0).fit(data, target)
#     return search


@timer
def hyperparameter_optimization_with_categorical(data, target, cat_features, n_iter=250, task_type="GPU", devices='0'):
    param_grid = {'iterations': randint(low=100, high=50000),
                  'learning_rate': uniform(location=1e-5, scale=2),
                  'depth': randint(low=2, high=14),
                  'l2_leaf_reg': uniform(location=1e-4, scale=1e2),
                  'bagging_temperature': uniform(location=0, scale=10)}
    cat = CatBoostClassifier(silent=True, task_type=task_type, boosting_type='Ordered', bootstrap_type='Bayesian',
                             random_seed=1, devices=devices, loss_function='AUC', cat_features=cat_features)
    results = cat.randomized_search(param_grid, X=data, y=target, plot=False, verbose=0, shuffle=False,
                                    partition_random_seed=1, n_iter=n_iter, cv=5)
    return cat, results['params']
